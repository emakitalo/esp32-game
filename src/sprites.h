#pragma once

#ifndef SPRITES_H
#define SPRITES_H

const bool spritePlayer[9] = {
    0, 0, 1,
    1, 1, 0,
    0, 0, 1};

const bool spriteEnemy[9] = {
    0, 1, 0,
    1, 1, 1,
    0, 1, 0};

const bool spriteAmmo[2] = {1, 1};

const bool spriteBoom[9] = {
    1, 0, 1,
    0, 1, 0,
    1, 0, 1};

#endif