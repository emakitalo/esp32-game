#include "sequences.h"

Sequence playerDeath = {
    false,
    1,
    0,
    new const bool *[2]
    { spritePlayer, spriteBoom }};

Sequence enemyDeath = {
    true,
    1,
    0,
    new const bool *[2]
    { spriteEnemy, spriteBoom }};

Sequence ammoDefault = {
    false,
    4,
    0,
    new const bool *[1]
    { spriteAmmo }};