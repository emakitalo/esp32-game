#pragma once
#ifndef GAME_H
#define GAME_H

#include "level.h"
#include "menu.h"

enum GAME_STATE
{
    MENU,
    PAUSE,
    INGAME,
    LEVEL_PASSED,
    LEVEL_INIT
};

extern enum GAME_STATE game_state;
extern void updateGame();

#endif