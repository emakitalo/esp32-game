#pragma once

#ifndef CONTROLLER_H
#define CONTROLLER_H

const unsigned char BUTTON_COUNT = 4;

typedef struct {
  unsigned char id;
  bool state;
} Button;

typedef struct {
  Button buttons[4];
} Controller;

extern Controller controller;

void initController(void);
void updateController(void);

#endif
