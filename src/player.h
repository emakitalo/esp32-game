#pragma once
#ifndef PLAYER_H
#define PLAYER_H

#include "timer.h"
#include "display.h"
#include "game.h"
#include "sound.h"
#include "entity.h"
#include "level.h"
#include "game.h"

void updatePlayer(Entity *e);

#endif