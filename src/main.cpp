#include <Arduino.h>
#include "wifi.h"
#include "display.h"
#include "controller.h"
#include "sound.h"
#include "game.h"

const int LED_PIN = 16;

unsigned long int timer;
unsigned long int delta_time;

void setup()
{
  Serial.begin(115200);
  pinMode(LED_PIN, OUTPUT);

  initWIFI();
  initController();
  initScreen();
  initAudio();

  timer = millis();
  delta_time = millis();
}

void loop()
{
  updateController();
  if ((millis() - timer) > FRAME_RATE_MS)
  {
    updateGame();
    timer = millis();
  }
  updateAudioOut();
  delta_time = millis() - delta_time;
  // ArduinoOTA.handle();
}
