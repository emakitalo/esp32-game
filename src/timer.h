#pragma once

#ifndef TIMER_H
#define TIMER_H

struct Timer
{
  unsigned long duration;
  unsigned long age;
};

#endif