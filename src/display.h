#pragma once

#ifndef DISPLAY_H
#define DISPLAY_H

#include <Arduino.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

#define OLED_RESET     4 // Reset pin # (or -1 if sharing Arduino reset pin)
#define SCREEN_ADDRESS 0x3C ///< See datasheet for Address; 0x3D for 128x64, 0x3C for 128x32

const uint8_t LEDMATRIX_CS_PIN = 15;
const int LEDMATRIX_SEGMENTS = 32;
const int LEDMATRIX_HEIGHT = 128;
const int LEDMATRIX_WIDTH = LEDMATRIX_SEGMENTS * LEDMATRIX_HEIGHT;

const int FRAME_RATE = 30;
const int FRAME_RATE_MS = 1000 / FRAME_RATE;

extern Adafruit_SSD1306 lmd;

void initScreen(void);

#endif
