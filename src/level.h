#pragma once

#ifndef LEVEL_H
#define LEVEL_H

#include <Arduino.h>
#include "display.h"
#include "timer.h"
#include "entity.h"
#include "player.h"
#include "sound.h"

struct Level
{
    const unsigned char id;
    unsigned int score;
    const unsigned char entitiesCount;
    const unsigned char enemyCount;
    const unsigned char enemyOffset;
    const unsigned char ammoCount;
    const unsigned char ammoOffset;
    unsigned char enemiesLeft;
    unsigned char activeEnemies;
    Timer spawnTimer;
    struct Entity *entities;
};

extern struct Level *level;
extern unsigned char activeLevel;
extern bool level_passed;
extern bool level_game_over;

void updateLevel(Level *l);
Level *createLevel(unsigned char id);
void draw(Entity *e);
Entity *checkCollision(Entity *e, Level *l);
void updateEntities(Level *l);
void spawnEnemy(Level *l);
void shoot(Level *l, char pId);

#endif