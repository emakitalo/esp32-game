#include "menu.h"

char msg[11] = "HelloWorld";
char *score_value = NULL;
const MenuButton start = {msg, 0, 0, 0, 0};
const MenuButton score = {score_value, 0, 0, 0, 0};

Menu menu = {
    0,
    new MenuPage[2]{
        {msg, new MenuButton[1]{start}},
        {"Score", new MenuButton[1]{score}}
        },
    true
    };

void initMenu()
{
    lmd.setFont(&FreeMonoBoldOblique12pt7b);
}

void drawPage(MenuPage p)
{

    lmd.setTextSize(1);              // Normal 1:1 pixel scale
    lmd.setTextColor(SSD1306_WHITE); // Draw white text
    lmd.setCursor(0, 0);             // Start at top-left corner
    lmd.cp437(true);                 // Use full 256 char 'Code Page 437' font

    p.header != NULL ? lmd.println(p.header) : NULL;
    const unsigned char cnt = sizeof(p.buttons) / sizeof(&p.buttons[0]);
    for (unsigned char i = 0; i < cnt; i++)
    {
        lmd.println(p.buttons[i].text);
    }
}

void handleMenuInput(Menu m)
{
    if (controller.buttons[0].state == LOW)
    {
        game_state = LEVEL_INIT;
    }
}
