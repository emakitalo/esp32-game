#pragma once

#ifndef SEQUENCES_H
#define SEQUENCES_H

#include <Arduino.h>
#include "sprites.h"

struct Sequence
{
    bool playing;
    unsigned char rate;
    unsigned char frame;
    const bool **sprites;
};

extern struct Sequence playerDeath;
extern struct Sequence enemyDeath;
extern struct Sequence ammoDefault;

#endif