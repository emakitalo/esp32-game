#include "entity.h"

Entity createEntity(Entities type, char pId)
{
  switch (type)
  {
  case Player:
    return {type, pId, 3, 3, LEDMATRIX_HEIGHT - 3, 0, 0, 0, -2, 2, 1, 1, 0, 1, new Sequence[1]{playerDeath}, StatePlayerDefault, 2, sound_pool, NULL, {100, millis()}};
    break;
  case Enemy:
    return {type, pId, 3, 3, 0, 0, 0, 0, 2, 2, 1, 0, 0, 1, new Sequence[1]{enemyDeath}, StateIdle, 1, sound_pool, NULL, {100, millis()}};
    break;
  case Ammo:
    return {type, pId, 2, 1, 0, 0, 0, 0, 4, 4, 1, 0, 0, 1, new Sequence[1]{ammoDefault}, StateIdle, 1, sound_pool, NULL, {100, millis()}};
    break;
  }
}

void updateDefault(Entity *e)
{
  Sequence *s = &e->sequences[e->activeSequence];
  s->frame = (s->frame + 1) % 2;
  if (e->y > LEDMATRIX_SEGMENTS - e->height || e->y < 0)
  {
    e->dy *= -1;
    if(e->timer.duration > 0)
      e->timer.duration -= 5;
    if (e->x < LEDMATRIX_HEIGHT)
    {
      e->x += e->dx * e->width;
    }
  }
  e->y += e->dy * e->speed;
}

void updateAmmo(Entity *e)
{
  if (e->x > -e->width && e->x < LEDMATRIX_HEIGHT)
  {
    e->x -= e->dx * e->speed;
  }
  else
  {
    e->active = false;
  }
}