#include <Arduino.h>
#include "controller.h"

Controller controller = {
  {
    {32, false},
    {33, false},
    {17, false},
    {16, false}
  }
};

void initController() {
  for (unsigned char i = 0; i < BUTTON_COUNT; i++) {
    pinMode(controller.buttons[i].id, INPUT_PULLUP);
  }
}

void updateController() {
  for (unsigned char i = 0; i < BUTTON_COUNT; i++) {
    controller.buttons[i].state = digitalRead(controller.buttons[i].id);
  }
}