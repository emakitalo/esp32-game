#include "display.h"

Adafruit_SSD1306 lmd(LEDMATRIX_HEIGHT, LEDMATRIX_SEGMENTS, &Wire, OLED_RESET);

void initScreen() {
  // SSD1306_SWITCHCAPVCC = generate display voltage from 3.3V internally
  if (!lmd.begin(SSD1306_SWITCHCAPVCC, SCREEN_ADDRESS)) {
    Serial.println(F("SSD1306 allocation failed"));
    for (;;); // Don't proceed, loop forever
  }
}