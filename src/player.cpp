#include "player.h"

Timer shootTimer = {
    300,
    millis()};

bool autofire = false;

void updatePlayer(Entity *e)
{
  // Up
  if (controller.buttons[1].state == LOW)
  {
    if (e->x > -e->width)
      e->x += e->dx * e->speed;
  }
  else
  {
    if (e->x < LEDMATRIX_HEIGHT - e->width)
      e->x -= e->dx * e->speed;
  }
  if (controller.buttons[2].state == LOW)
  {
    if (e->y > 0)
      e->y -= e->dy * e->speed;
  }
  if (controller.buttons[3].state == LOW)
  {
    if (e->y < LEDMATRIX_SEGMENTS - e->height - e->dy)
      e->y += e->dy * e->speed;
  }
  if (controller.buttons[0].state == LOW)
  {
    if (millis() - shootTimer.age > shootTimer.duration)
    {
      shoot(level, 0);
      shootTimer.age = millis();
    } else if(!autofire) {
      shoot(level, 0);
    }
    autofire = true;
  } else {
    autofire = false;
  }
}