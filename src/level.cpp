#include "level.h"

unsigned char activeLevel = 0;
bool level_passed = false;
bool level_game_over = false;
Level *level = createLevel(activeLevel);


Timer shoot_multiplier = {
    300,
    0};

Level *createLevel(unsigned char id)
{
  switch (id)
  {
  case 0:
    return new Level{
        id,
        0,
        10,
        4,
        1,
        5,
        4 + 1,
        48,
        0,
        {FRAME_RATE_MS * FRAME_RATE * 4, millis()},
        new Entity[10]{
            createEntity(Player, 0),
            createEntity(Enemy, 1),
            createEntity(Enemy, 2),
            createEntity(Enemy, 3),
            createEntity(Enemy, 4),
            createEntity(Ammo, 0),
            createEntity(Ammo, 0),
            createEntity(Ammo, 0),
            createEntity(Ammo, 0),
            createEntity(Ammo, 0)}};
    break;
  }
}

Entity *checkCollision(Entity *e, Level *l)
{
  for (unsigned char i = 0; i < l->entitiesCount; i++)
  {
    Entity *e2 = &l->entities[i];
    int e_x = e->x + ((e->x_prev - e->x) >> 1);
    int e_y = e->y + ((e->y_prev - e->y) >> 1);
    int e2_x = e2->x + ((e2->x_prev - e2->x) >> 1);
    int e2_y = e2->y + ((e2->y_prev - e2->y) >> 1);

    if (e_x < e2_x + e2->width && e_x + e->width > e2_x && e_y < e2_y + e2->height && e->height + e_y > e2_y && e != e2 && e->active && e2->active)
    {
      return e2;
    }
  }
  return NULL;
}

void shoot(Level *l, char pId)
{
  for (unsigned char i = 0; i < l->ammoCount; i++)
  {
    Entity *ammo = &l->entities[l->ammoOffset + i];
    if (!ammo->active)
    {
      ammo->parentId = pId;
      Entity *parent = &l->entities[ammo->parentId];
      ammo->x = parent->x - (parent->dx > 0 ? -ammo->width : ammo->width);
      ammo->y = parent->y + parent->height / 2;
      ammo->x_prev = ammo->x;
      ammo->y_prev = ammo->y;
      ammo->y = parent->y + parent->height / 2;
      ammo->dx = (ammo->dx < 0 ? -ammo->dx : ammo->dx) * (parent->dx > 0 ? -1 : 1);
      ammo->dy = parent->dy;
      ammo->health = 1;
      ammo->active = true;
      ammo->sounds[0].play = true;
      break;
    }
  }
}

void spawnEnemy(Level *l)
{
  if (millis() - l->spawnTimer.age > l->spawnTimer.duration && l->activeEnemies < l->enemyCount)
  {
    for (char i = 0; i < l->enemyCount; i++)
    {
      Entity *e = &l->entities[l->enemyOffset + i];
      if (!e->active)
      {
        e->active = true;
        e->health = 2;
        e->x = 0;
        e->y = 0;
        e->x_prev = 0;
        e->y_prev = 0;
        e->dx = 1;
        e->dy = -1;
        l->activeEnemies++;
        l->spawnTimer.age = millis();
        break;
      }
    }
  }
}

void updateEntities(Level *l)
{
  Entity *e;
  unsigned char i;
  for (i = 0; i < l->entitiesCount; i++)
  {
    e = &l->entities[i];
    if (e->active)
    {
      e->hit_from = checkCollision(e, l);
    }
  }

  for (i = 0; i < l->entitiesCount; i++)
  {
    e = &l->entities[i];
    if (e->hit_from != NULL)
    {
      if (e->hit_from->parentId != e->parentId)
      {
        e->health--;
      }
    }

    if (e->health <= 0 && e->active)
    {
      switch (e->type)
      {
      case Player:
        break;
      case Enemy:
        e->sounds[1].play = true;
        l->enemiesLeft--;
        l->activeEnemies--;
        break;
      case Ammo:
        if (e->hit_from != NULL)
        {
          if (e->hit_from->active)
            sound_pool[2].play = true;
          Entity *parent = &l->entities[e->parentId];
          if (e->hit_from->type == Enemy && parent != NULL) {
            if(parent->type == Player)
              l->score++;
          }
        }
        break;
      }
      e->active = false;
    }

    if (e->active)
    {
      e->x_prev = e->x;
      e->y_prev = e->y;
      draw(e);
      switch (e->state)
      {
      case StatePlayerDefault:
        updatePlayer(e);
        break;
      case StateIdle:
        switch (e->type)
        {
        case Player:
          break;
        case Enemy:
          if (millis() - e->timer.age > e->timer.duration)
          {
            updateDefault(e);
            if (millis() % 128 > 120)
            {
              shoot(l, i);
            }
            e->timer.age = millis();
          }
          break;
        case Ammo:
          updateAmmo(e);
          break;
        }
        break;
      default:
        updateDefault(e);
        break;
      }
    }

    if (e->hit_from != NULL)
      e->hit_from = NULL;
  }
}

void updateLevel(Level *l)
{
  switch (l->id)
  {
  case 0:
    spawnEnemy(l);
    break;
  }
  updateEntities(l);

  if (l->enemiesLeft <= 0)
    level_passed = true;
  if(l->entities[0].health <= 0)
    level_game_over = true;
}

void draw(Entity *e)
{
  for (unsigned char i = 0; i < e->width; i++)
  {
    for (unsigned char j = 0; j < e->height; j++)
    {
      const unsigned char idx = i + j * e->width;
      const Sequence *s = &e->sequences[e->activeSequence];
      const int x = e->x + i;
      const int y = e->y + j;
      if (x < LEDMATRIX_WIDTH && x >= 0 && y < LEDMATRIX_HEIGHT && y >= 0)
        lmd.drawPixel(x, y, s->sprites[s->frame][idx]);
    }
  }
}