#include "sound.h"

// for triggering the envelope

byte gain;
Q8n8 mod_to_carrier_ratio = float_to_Q8n8(3.f);
float master_volume = 2.f;
bool play = false;

AudioDelayFeedback <CONTROL_RATE, ALLPASS> aDel;

unsigned char sounds_length = 4;

EventDelay delay_1;

FMSynth *synths = new FMSynth[4]{
  {0.005f, 0,0,0,(SIN256_DATA),(SIN256_DATA),(SIN256_DATA)},
  {0.005f, 0,0,0,(SIN256_DATA),(SIN256_DATA),(SIN256_DATA)},
  {0.0001f, 0,0,0,(SIN256_DATA),(SIN256_DATA),(SIN256_DATA)},
  {0.002f, 0,0,0,(SIN256_DATA),(SIN256_DATA),(SIN256_DATA)}
};

ADSRConf *sound_pool = new ADSRConf[sounds_length]{
  {false, 94, 12, 4, 5, 250, 50, 25, 0, 0, ADSR<CONTROL_RATE, CONTROL_RATE>(), &synths[0]}, 
  {false, 72, 24, 4, 0, 25, 100, 25, 0, 0, ADSR<CONTROL_RATE, CONTROL_RATE>(), &synths[1]},
  {false, 60, 48, 16, 20, 5, 500, 50, 0, 0, ADSR<CONTROL_RATE, CONTROL_RATE>(), &synths[2]},
  {false, 0, 32, 8, 50, 25, 100, 500, 0, (unsigned char) 2, ADSR<CONTROL_RATE, CONTROL_RATE>(), &synths[3]}
};

void initAudio()
{
  randSeed(); // fresh random
  startMozzi(CONTROL_RATE);
  delay_1.set(10000);
  aDel.setDelayTimeCells(AUDIO_RATE * 0.75f);
  aDel.setFeedbackLevel(-32);
  sound_pool[1].play = true;
}

void handleEnvelope(ADSRConf *c)
{
  c->envelope.noteOff();
  c->envelope.setADLevels(
      c->attack_level,
      c->decay_level);
  c->envelope.setTimes(
      c->attack,
      c->decay,
      c->sustain,
      c->release_ms);
  c->envelope.noteOn();
}

void handleFrequencies(ADSRConf *c) {
  c->synth->c_freq = mtof(c->note);
  c->synth->m_freq = (c->synth->c_freq * mod_to_carrier_ratio) >> 8; 
  c->synth->i_dev = c->synth->m_freq * c->synth->intensity.next() >> 8; 
  c->synth->carrier.setFreq(c->synth->c_freq);
  c->synth->modulator.setFreq(c->synth->m_freq);
  c->synth->intensity.setFreq((c->attack + c->decay + c->sustain + c->release_ms) * c->synth->intensity_mult);
}

void updateControl()
{
  if(delay_1.ready()) {
    uint8_t r = rand(255);
    delay_1.set(100 + (r << 2));
    sound_pool[3].note = 48 + ((sound_pool[3].note + r) % 12) - 6;
    sound_pool[3].play = true;
    delay_1.start();
  }

  for (unsigned char i = 0; i < sounds_length; i++)
  {
    ADSRConf *snd = &sound_pool[i];
    if (snd->play)
    {
      handleEnvelope(snd);
      snd->synth->carrier.setPhase(0);
      snd->synth->modulator.setPhase(0);
      snd->synth->intensity.setPhase(0);
      snd->play = false;
    }
    handleFrequencies(snd);
    snd->envelope.update();
    snd->gain = snd->use_static_gain ? 
      (snd->use_static_gain + snd->envelope.next()) >> 1 
      : snd->envelope.next();
  }
}

AudioOutput_t updateAudio()
{
  int snd_out = 0;
  for(unsigned char i=0; i < sounds_length; i++) {
    ADSRConf *snd = &sound_pool[i];
    int modulation = snd->synth->i_dev * snd->synth->modulator.next();
    snd_out += snd->gain * snd->synth->carrier.phMod(modulation);
  }
  return MonoOutput::from16Bit(aDel.next((int)(snd_out * master_volume) >> sounds_length));
}

void updateAudioOut()
{
  audioHook();
}