#pragma once

#ifndef ENTITIES_H
#define ENTITIES_H

#include <Arduino.h>
#include "sound.h"
#include "sequences.h"
#include "timer.h"
#include "controller.h"
#include "display.h"

struct Level;

enum State
{
  StatePlayerDefault,
  StateIdle
};

enum Actions
{
  Idle,
  Shoot
};

enum Entities
{
  Player,
  Enemy,
  Ammo
};

struct Entity
{
  const Entities type;
  unsigned char parentId;
  const char width;
  const char height;
  signed int x;
  signed int y;
  signed int x_prev;
  signed int y_prev;
  signed char dx;
  signed char dy;
  unsigned char speed;
  bool active;
  unsigned char activeSequence;
  const unsigned char sequencesLength;
  Sequence *sequences;
  State state;
  unsigned char health;
  ADSRConf *sounds;
  Entity *hit_from;
  Timer timer;
};

void updateAmmo(Entity *e);
void updateDefault(Entity *e);
Entity createEntity(Entities type, char pId);

#endif