#pragma once
#ifndef MENU_H
#define MENU_H

#include "game.h"
#include "controller.h"
#include "display.h"
#include <Fonts/FreeMonoBoldOblique12pt7b.h>

struct MenuButton {
    const char *text;
    int16_t  x1;
    int16_t y1;
    uint16_t w;
    uint16_t h;
};

struct MenuPage {
    char *header;
    MenuButton *buttons;
};

struct Menu {
    unsigned char active_page;
    MenuPage *pages;
    bool update;
};

extern Menu menu;

void initMenu(void);
void drawPage(MenuPage page);
void handleMenuInput(Menu m);

#endif