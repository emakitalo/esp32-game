#pragma once

#ifndef SOUND_H
#define SOUND_H

#include <Arduino.h>
#include <MozziGuts.h> // this makes everything work
#include <Oscil.h>  // a template for an oscillator
#include <tables/sin256_int8.h>  // a wavetable holding a sine wave
#include <AudioDelayFeedback.h>
#include <EventDelay.h>
#include <ADSR.h>
#include <mozzi_rand.h>
#include <mozzi_midi.h>

#define CONTROL_RATE 64

struct FMSynth {
    float intensity_mult;
    int c_freq;
    int m_freq;
    int i_dev;
    Oscil<256, AUDIO_RATE> carrier;
    Oscil<256, AUDIO_RATE> modulator;
    Oscil<256, CONTROL_RATE> intensity;
};

struct ADSRConf
{
    bool play;
    unsigned char note;
    unsigned char attack_level;
    unsigned char decay_level;
    int attack;
    int decay;
    int sustain;
    int release_ms;
    signed char gain;
    signed char use_static_gain;
    ADSR<CONTROL_RATE, CONTROL_RATE> envelope;
    FMSynth *synth;
};

extern ADSRConf *sound_pool;
extern FMSynth *synths;

void initAudio(void);
void updateAudioOut(void);


#endif
