#include "game.h"

enum GAME_STATE game_state = MENU;
unsigned int score = 0;
char score_txt[8];

void updateGame()
{
    switch (game_state)
    {
    case MENU:
        if (menu.update)
        {
            lmd.clearDisplay();
            lmd.setRotation(3);
            drawPage(menu.pages[menu.active_page]);
            lmd.display();
            lmd.setRotation(0);
            menu.update = false;
        }
        handleMenuInput(menu);
        break;
    case PAUSE:
        break;
    case LEVEL_INIT:
        for (unsigned char i = 0; i < level->entitiesCount; i++)
        {
            free(level->entities[i].sequences);
        }
        free(level->entities);
        level = createLevel(activeLevel);
        game_state = INGAME;
        break;
    case LEVEL_PASSED:
        menu.update = true;
        score += level->score;
        utoa(score, score_txt, 10);
        menu.active_page = 1;
        menu.pages[menu.active_page].buttons[0].text = score_txt;
        game_state = MENU;
        if (level_game_over)
        {
            activeLevel = 0;
            score = 0;
            level_game_over = false;
        }
        break;
    case INGAME:
        if (level)
        {
            lmd.clearDisplay();
            updateLevel(level);
            lmd.display();
            if (level_passed || level_game_over)
            {
                level_passed = false;
                game_state = LEVEL_PASSED;
            }
        }
        break;
    }
}